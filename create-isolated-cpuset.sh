#!/bin/bash
# From SO: http://stackoverflow.com/questions/9072060/one-core-exclusively-for-my-process

if [[ $# -ne 1 ]]; then
	echo "Illegal number of parameters. You must provide 1 parameter with the name of the executable to run on the isolated cpuset"
	exit 1
fi
if [ "$(whoami)" != "root" ]; then
	echo "You need to have root privileges."
	exit 1
fi

mkdir /cpuset
mount -t cpuset none /cpuset/

mkdir /cpuset/sys                                   # create sub-cpuset for system processes
/bin/echo 0-2 > /cpuset/sys/cpus             # assign cpus (cores) 0-2 to this set
                                            # adjust if you have more/less cores
/bin/echo 1 > /cpuset/sys/cpu_exclusive
/bin/echo 0 > /cpuset/sys/mems

mkdir /cpuset/rt                                    # create sub-cpuset for my process
/bin/echo 3 > /cpuset/rt/cpus                # assign cpu (core) 3 to this cpuset
                                            # adjust this to number of cores-1
/bin/echo 1 > /cpuset/rt/cpu_exclusive
/bin/echo 0 > /cpuset/rt/mems
/bin/echo 0 > /cpuset/rt/sched_load_balance
/bin/echo 1 > /cpuset/rt/mem_hardwall

# move all processes from the default cpuset to the sys-cpuset
for T in `cat /cpuset/tasks`; do echo "Moving " $T; /bin/echo $T > /cpuset/sys/tasks; done

# disable interruptions for the isolated cpu
find /proc/irq/ -name "smp_affinity" -exec sh -c "echo 7 > {}" \;

# set fixed frequency for the isolated cpu
cpufreq-selector -c 3 -f 800000

# start task in specific isolated cpu
$1 & /bin/echo $! > /cpuset/rt/tasks 	# Move the just created task PID to the tasks of the exclusive CPU
