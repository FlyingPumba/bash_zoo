#!/bin/bash
if [ "$(whoami)" != "root" ]; then
	echo "You need to have root privileges."
	exit 1
fi

wget https://atom.io/download/deb -O atom-amd64.deb
dpkg -i atom-amd64.deb
rm atom-amd64.deb

