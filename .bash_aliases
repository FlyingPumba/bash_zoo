alias i='sudo apt-get install'
alias r='sudo apt-get remove'
alias s='apt-cache search'
alias updt='sudo apt-get update'
alias upgr='sudo apt-get upgrade'
alias srcdir='cd ~/src/'
alias editaliases='sudo vim ~/.bash_aliases'
alias resourcebashrc='source ~/.bashrc'
alias androidstudio='~/android-studio/bin/studio.sh'
alias adb='~/android-sdk/platform-tools/adb'
alias gopen='gnome-open'
alias gsound='gnome-sound-applet'
alias clear='printf "\033c"'
alias eclipse='/opt/eclipse/eclipse'
alias openBackup='sudo cryptsetup luksOpen /dev/sdb backup'
alias mountBackup='sudo mount /dev/mapper/backup /mnt/backup/'
alias umountBackup='sudo umount /mnt/backup'
alias closeBackup='sudo cryptsetup luksClose /dev/mapper/backup'
alias another-bash='gnome-terminal --working-directory=$(pwd)'
alias ..='cd ..'

alias mouseleft="xmodmap -e 'pointer = 3 2 1'"
alias mouseright="xmodmap -e 'pointer = 1 2 3'"

alias fpp="/home/ivan/src/PathPicker/fpp"
alias pull-origin-master='git pull origin master'
alias push-origin-master='git push origin master'

alias ssh-milagro-dc='ssh iarcuschin@milagro.dc.uba.ar'
