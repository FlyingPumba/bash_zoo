#!/bin/bash
# From SO: http://stackoverflow.com/questions/9072060/one-core-exclusively-for-my-process

if [ "$(whoami)" != "root" ]; then
        echo "You need to have root privileges."
        exit 1
fi

# Removing the isolated cpuset can be accomplished also by restartin the system

# move tasks back from sys-cpuset to root cpuset
for T in `cat /cpuset/sys/tasks`; do echo "Moving " $T; /bin/echo $T > /cpuset/tasks; done
# remove sys-cpuset
rmdir /cpuset/sys
# move tasks back from rt-cpuset to root cpuset
for T in `cat /cpuset/rt/tasks`; do echo "Moving " $T; /bin/echo $T > /cpuset/tasks; done
# remove rt-cpuset
rmdir /cpuset/rt
# unmount and remove /cpuset
umount /cpuset
rmdir /cpuset

# re-enabling interruptions for the isolated cpu
find /proc/irq/ -name "smp_affinity" -exec sh -c "echo f > {}" \;
